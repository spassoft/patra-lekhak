// Bulk generation should happen in batches.
// - schedule time based triggers
// - delete the processed rows or move them to log sheet

// Debug why form trigger gets disabled.
// Delete trigger when the form is deleted.

// UI Improvements:
//    Section headers should be more visible.
// Improve Logging
// Improve error reporting
// Help clues
// handle date filling in i18n way
// Test functions
// Support Presentation type
// Logo issues
// test spreadsheet based add-on
// test on Google Drawing based document
// test on Google Presentation based document
// Write the terms of use
// Submit to Google.
