/**
 * This file contains function to maintain internal state in the add-on.
 *
 * @author paridhi.chandra@gmail.com
 */

STRINGS = {
  LANGUAGE: "PatraLekhak-Property-LanguageOfChoice",
  TEMPLATE_LIST_PROPERTY: "PatraLekhak-Property-ListOfFormLessTemplates",
  FORM_LIST_PROPERTY: "PatraLekhak-Property-ListOfForms",
  SSFORM_LIST_PROPERTY: "PatraLekhak-Property-ListOfSSForms",
  TT_QUEUE_PROPERTY: "PatraLekhak-Property-TimeTriggerQueue",
  TT_TRIGGER_PROPERTY: "PatraLekhak-Property-TimeTrigger",
  FORM_TEMPLATE_MAP_PROPERTY: "PatraLekhak-Property-FormToTemlateMap"
};

function setLanguage(lang) {
  PropertiesService.getUserProperties().setProperty(STRINGS.LANGUAGE, lang);
}

function getLanguage() {
  return PropertiesService.getUserProperties().getProperty(STRINGS.LANGUAGE);
}

function getProperties() {
  return PropertiesService.getScriptProperties();
}

function addTemplateWithoutForm(templateId) {
  addToList(templateId, STRINGS.TEMPLATE_LIST_PROPERTY);
}

function addForm(formId, ssformId, templateId) {
  addToList(formId, STRINGS.FORM_LIST_PROPERTY);
  addToList(ssformId, STRINGS.SSFORM_LIST_PROPERTY);
  linkFormToTemplate(formId, ssformId, templateId);
}

function addToList(id, listName) {
  var list = JSON.parse(getProperties().getProperty(listName));
  if (!list) {
    list = [];
  }
  
  for (var i = 0; i < list.length; i++) {
    if (list[i] == id) {
      return;
    }
  }
  list.push(id);
  getProperties().setProperty(listName, JSON.stringify(list));
}

function getTemplatesWithoutForm() {
  return JSON.parse(getProperties().getProperty(STRINGS.TEMPLATE_LIST_PROPERTY));
}

function getForms() {
  return JSON.parse(getProperties().getProperty(STRINGS.FORM_LIST_PROPERTY));
}

function getSSForms() {
  return JSON.parse(getProperties().getProperty(STRINGS.SSFORM_LIST_PROPERTY));
}

function removeTemplateWithoutForm(templateId) {
  Logger.log(templateId);
  removeFromList(templateId, STRINGS.TEMPLATE_LIST_PROPERTY);
}

function removeFromList(id, listName) {
  var list = JSON.parse(getProperties().getProperty(listName));
  Logger.log(list);
  if (!list) {
    list = [];
  }
  
  for (var i = 0; i < list.length; i++) {
    if (list[i] == id) {
      break;
    }
  }
  if (i < list.length) {
    list.splice(i, 1);
    Logger.log(list);
    getProperties().setProperty(listName, JSON.stringify(list));
  }
  Logger.log(JSON.parse(getProperties().getProperty(listName)));
}

function reshuffleFormList(formId) {
  reshuffleList(formId, STRINGS.FORM_LIST_PROPERTY);
}

function reshuffleSSFormList(formId) {
  reshuffleList(formId, STRINGS.SSFORM_LIST_PROPERTY);
}

function reshuffleList(formId, listName) {
  var list = JSON.parse(getProperties().getProperty(listName));
  if (!list) {
    return;
  }

  for (var i = 0; i < list.length; i++) {
    if (list[i] == formId) {
      break;
    }
  }
  if (i < list.length) {
    list.splice(i, 1);
    list.unshift(formId);
    getProperties().setProperty(STRINGS.FORM_LIST_PROPERTY, JSON.stringify(list));
  }
}

function linkFormToTemplate(formId, ssformId, templateId) {
  var map = JSON.parse(getProperties().getProperty(STRINGS.FORM_TEMPLATE_MAP_PROPERTY));
  if (!map || Object.keys(map).length == 0) {
    map = [];
  }
  for (var member of map) {
    if (member.formId == formId) {
      return;
    }
  }
  map.push({formId: formId, ssformId: ssformId, templateId: templateId});
  getProperties().setProperty(STRINGS.FORM_TEMPLATE_MAP_PROPERTY, JSON.stringify(map));
}

function getMappingForTemplate(templateId) {
  return getMapping(templateId, "templateId");
}

function getMappingForForm(formId) {
  return getMapping(formId, "formId");
}

function getMappingForSSForm(ssformId) {
  return getMapping(ssformId, "ssformId");
}

function getMapping(id, fieldToSearch) {
  var map = JSON.parse(getProperties().getProperty(STRINGS.FORM_TEMPLATE_MAP_PROPERTY));
  if (!map || Object.keys(map).length == 0) {
    return null;
  }
  for (var member of map) {
    if (member[fieldToSearch] == id) {
      return member;
    }
  }
  return null;
}

function unlinkTemplate(formId) {
  var map = JSON.parse(getProperties().getProperty(STRINGS.FORM_TEMPLATE_MAP_PROPERTY));
  if (Object.keys(map).length == 0) {
    return;
  }
  for (var i = 0; i < map.length; i++) {
    if (map[i].formId == formId) {
      break;
    }
  }
  if(i < map.length) {
    map.splice(i, 1);
    getProperties().setProperty(STRINGS.FORM_TEMPLATE_MAP_PROPERTY, JSON.stringify(map));
  }
}

/*
 * Figure out if one file from the triplet is deleted, remove the whole triplet.
 */
function cleanupMap() {
  var map = JSON.parse(getProperties().getProperty(STRINGS.FORM_TEMPLATE_MAP_PROPERTY));
  if (!map || Object.keys(map).length == 0) {
    return;
  }
  for (var i = 0; i < map.length; i++) {
    try {
      if (DriveApp.getFileById(map[i].templateId).isTrashed() ||
          DriveApp.getFileById(map[i].formId).isTrashed() ||
          DriveApp.getFileById(map[i].ssformId).isTrashed()) {
        throw Error("");
      }
    } catch (error) {
      cleanupOneSet(map[i]);
    }
  }
}

/*
 * Some files have been deleted, so that set is broken. Cleanup.
 * 1. Remove the trigger
 * 2. Delete remaining files
 * 3. Remove these files from internal file list
 * 4. Remove these files from internal map.
 */
function cleanupOneSet(mapEntry) {
  deleteTrigger(mapEntry.formId);
  try {
    removeFileById(mapEntry.templateId);
  } catch (error) {
    // if the file is not available, our job is done. ignore the error
  }
  try {
    removeFileById(mapEntry.formId);
  } catch (error) {
    // if the file is not available, our job is done. ignore the error
  }
  try {
    removeFileById(mapEntry.ssformId);
  } catch (error) {
    // if the file is not available, our job is done. ignore the error
  }
  removeFromList(mapEntry.templateId, STRINGS.TEMPLATE_LIST_PROPERTY);
  removeFromList(mapEntry.formId, STRINGS.FORM_LIST_PROPERTY);
  removeFromList(mapEntry.ssformId, STRINGS.SSFORM_LIST_PROPERTY);
  unlinkTemplate(mapEntry.formId);
}

function dumpConfigState() {
  Logger.log("\nTemplates without forms:");
  printList(getTemplatesWithoutForm());
  Logger.log("\nForms:");
  printList(getForms());
  Logger.log("\nSpreadsheets:");
  printList(getSSForms());
  Logger.log("\nMappings:");
  printMap();
}

function printList(list) {
  for (var i = 0; i < list.length; i++) {
    Logger.log(DriveApp.getFileById(list[i]).getName());
  }
}

function printMap() {
  var map = JSON.parse(getProperties().getProperty(STRINGS.FORM_TEMPLATE_MAP_PROPERTY));
  for (var member of map) {
    var ssformName = "";
    if (member.ssformId) {
      ssformName = DriveApp.getFileById(member.ssformId).getName();
    }
    Logger.log("%s -> %s <- %s", DriveApp.getFileById(member.formId).getName(), DriveApp.getFileById(member.templateId).getName(), ssformName);
  }
}