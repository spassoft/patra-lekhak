/**
 * This file contains config params for the service.
 *
 * @author paridhi.chandra@gmail.com
 */

var serviceConfig = {
  truncatedListSize: 5
};