/**
 * This file contains regex definition for the add-on.
 *
 * @author paridhi.chandra@gmail.com
 */

REGEX_LIST = {
  CURRENT_DATE_REGEX: new RegExp(getStringInLocale(strCodes.CURRENT_DATE), "i"),
  DATE_REGEX: new RegExp(getStringInLocale(strCodes.DATE), "i"),
  FORM_FILENAME_SUFFIX_REGEX: new RegExp(getStringInLocale(strCodes.FORM_FILENAME_SUFFIX), "gi"),
  SSFORM_FILENAME_SUFFIX_REGEX: new RegExp(getStringInLocale(strCodes.SS_FORM_FILENAME_SUFFIX), "gi"),
  TEMPLATE_FILENAME_SUFFIX_REGEX: new RegExp(getStringInLocale(strCodes.TEMPLATE_FILENAME_SUFFIX), "gi")
};
