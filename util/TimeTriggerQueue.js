/**
 * Persistent queue to manage work for time based trigger.
 * One usecase is, when a bulk letter generation is triggerd, the work is
 * logged in the queue and processed slowly to prevent time-overrun.
 *
 * @author paridhi.chandra@gmail.com
 */
var triggerScheduled = isTTQueueEmpty();

function isTTQueueEmpty() {
  var queue = JSON.parse(getProperties().getProperty(STRINGS.TT_QUEUE_PROPERTY));
  return (!queue || queue.length == 0);
}

function getTTQueue() {
  var queue = JSON.parse(getProperties().getProperty(STRINGS.TT_QUEUE_PROPERTY));
  if (!queue || queue.length == 0) {
    return [];
  }
  return queue;
}

function setTTQueue(queue) {
  getProperties().setProperty(STRINGS.TT_QUEUE_PROPERTY, JSON.stringify(queue));
}

function appendToQueue(element) {
  var queue = getTTQueue();
  var i = findInQueue(element);
  if (i >= queue.length) {
    queue.push(element);
    setTTQueue(queue);
  }
}

function findInQueue(element) {
  var queue = getTTQueue();
  for(var i = 0; i < queue.length; i++) {
    if (queue[i].id === element.id) {
      return i;
    }
  }
  return i;
}

function removeFromQueue(element) {
  var queue = getTTQueue();
  var i = findInQueue(element);
  if (i < queue.length) {
    queue.splice(i, 1);
  }
  setTTQueue(queue);
}

function getFirstTTElement() {
  var queue = getTTQueue();
  if (queue.length == 0) {
    return null;
  }
  return queue[0];
}

function enqueueTT(element) {
  if (!getTimeTrigger()) {
    createTimeTrigger();
  }
  appendToQueue(element);
}

function dequeueTT(element) {
  removeFromQueue(element);
//  if (isTTQueueEmpty()) {
//    stopTimeTrigger();
//  }
}

function createTimeTrigger() {
  try {
  var trigger = ScriptApp.newTrigger("onTimeTrigger")
                         .timeBased()
                         .everyHours(1)
                         .create();
    getProperties().setProperty(STRINGS.TT_TRIGGER_PROPERTY, JSON.stringify(trigger));
  } catch (error) {
    Logger.log(error.message, error.name, error.toString());
    throw error;
  }
}

function getTimeTrigger() {
  var triggerData = JSON.parse(getProperties().getProperty(STRINGS.TT_TRIGGER_PROPERTY));
  var triggers = ScriptApp.getProjectTriggers();
  for (var i = 0; i < triggers.length; i++) {
    if (triggers[i].getUniqueId() == triggerData.uniqueId) {
      return triggers[i];
    }
  }
  return null;
}

function stopTrigger() {
  var trigger = getTimeTrigger();
  if (trigger) {
    ScriptApp.deleteTimeTrigger(trigger);
    getProperties.deleteProperty(STRINGS.TT_TRIGGER_PROPERTY);
  }
}

function onTimeTrigger() {
  Logger.log("Time based trigger fired");
  var element = getFirstTTElement();
  if (element) {
    this[element.func](element);
  }
}

function printTTQueue() {
  var queue = getTTQueue();
  var str = Utilities.formatString("%3d\n", queue.length);
  for (var member of queue) {
    str += Utilities.formatString("{id: %s\nfunc: %s}\n", member.id, member.func);
  }
  var trigger = JSON.parse(getProperties().getProperty(STRINGS.TT_TRIGGER_PROPERTY));
  str += Utilities.formatString("%s\n", trigger.toString());
  Logger.log(str);
}