/**
 * This file contains i18n related code for textual strings.
 *
 * @author paridhi.chandra@gmail.com
 */

strCodes = {
  APP_NAME: "an",
  CURRENT_DATE: "cd",
  DATE_SEPARATOR: "ds",
  DATE: "d",
  DEFAULT_TEMPLATE_NAME: "dtn",
  ERR_EMPTY_TEMPLATE: "eet",
  ERR_FORM_ALREADY_EXISTS: "efae",
  ERR_GENERIC: "eg",
  ERR_MISSING_FORM: "emf",
  ERR_NO_EMAIL: "ene",
  ERR_NOT_A_DOCUMENT: "enad",
  ERR_NOT_PL_FORM: "enpf",
  ERR_TEMPLATE_FILE: "etf",
  ERR_TEMPLATE_IS_TRASHED: "etit",
  ERR_TEMPLATE_NAME_EMPTY: "etne",
  ERROR_MAIL_SUBJECT: "ems",
  FILE_NAME_SEPARATOR: "fns",
  FORM: "f",
  FORM_FILENAME_SUFFIX: "ffns",
  GENERATED_FILE_PREFIX: "gfp",
  LOG: "l",
  MAIL_BODY: "mb",
  MAIL_SUBJECT: "ms",
  REPLACEMENT_MARKER_BEGIN: "rmb",
  REPLACEMENT_MARKER_END: "rme",
  SS_FORM_FILENAME_SUFFIX: "sffs",
  TEMPLATE: "t",
  TEMPLATE_FILENAME_SUFFIX: "tfs",
  UI_BUTTON_CREATE_TEMPLATE: "ubct",
  UI_FORM_SECTION_HEADING: "ufsh",
  UI_GENERATE_ACTION: "uga",
  UI_GENERATE_SECTION_HEADING: "ugsh",
  UI_MORE_BUTTON: "umb",
  UI_SSFORM_SECTION_HEADING: "ussh",
  UI_TEMPLATE_NAME_HINT: "utnh",
  UI_TEMPLATE_NAME_TITLE: "utnt",
  UI_TEMPLATE_SECTION_HEADING: "utsh"
};

var EN_US_APP_NAME = "Patra Lekhak";
var EN_US_FORM_SUFFIX = "Form";
var EN_US_SS_FORM_SUFFIX = "SSForm";
var EN_US_TEMPLATE_SUFFIX = "Template";
en_us = {
  "an": EN_US_APP_NAME,
  "cd": "CURRENT DATE",
  "d": "date",
  "ds": "-",
  "dtn": "New Template",
  "eet": "No placeholder found in the template.",
  "efae": "Form already exists: ",
  "eg": "There was an error: ",
  "emf": "The form is missing.\n",
  "ems": EN_US_APP_NAME + " - error caught",
  "enad": "Selected file is not a Document. Currently, only document based templates are supported",
  "ene": "Form should be configured to collect emails.",
  "enpf": "This is not a Patra-Lekhak form. There is no file template linked to this form.",
  "etf": "Error in template file: ",
  "etit": "Template is trashed",
  "etne": "No name provided for the template",
  "f": EN_US_FORM_SUFFIX,
  "ffns": " - " + EN_US_APP_NAME + " " + EN_US_FORM_SUFFIX,
  "fns": "-",
  "gfp": "patra-lekhak-letter-generated-on-",
  "l": "Log",
  "mb": "Your generated letter is attached.\n\n\nThis letter is generated by Patra-Lekhak utility written by Paridhi Gupta (Spas Enterprises).",
  "ms": "Your Generated Letter",
  "rmb": "{{",
  "rme": "}}",
  "sffs": " - " + EN_US_APP_NAME + " " + EN_US_SS_FORM_SUFFIX,
  "t": EN_US_TEMPLATE_SUFFIX,
  "tfs":  " - " + EN_US_APP_NAME + " " + EN_US_TEMPLATE_SUFFIX,
  "ubct": "Create A New Template",
  "ufsh": "<B>Single Letter Generation</B>\nClick on one of these forms to generate letters",
  "uga": "Generate",
  "ugsh": "GENERATE section\nClick on sheet name to trigger the letter generation",
  "umb": "More...",
  "ussh": "<B>Bulk Letter Generation</B>\nClick on sheet name to fill the form\nand then use following GENERATE section to trigger the letter generation",
  "utnh": "Give a name to your template file",
  "utnt": "Name of the template",
  "utsh": "<B>Template Creation</B>"
};

locale = "en-US";

function getStringInLocale(strId) {
  return en_us[strId];
}
