/**
 * Utility file for date related functions.
 *
 * @author paridhi.chandra@gmail.com
 */

function formatDate(date) {
  var months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  return "" + date.getDate() + "-" + months[date.getMonth()] + "-" + date.getFullYear();
}
