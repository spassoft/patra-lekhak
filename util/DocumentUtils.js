/**
 * This file has implementation of all those function which are DocumentApp specific.
 *
 * @author paridhi.chandra@gmail.com
 */

mimeHandlers[MimeType.GOOGLE_DOCS] = {
  create: createDocument,
  extractQuestions: extractQuestionsFromDocument,
  findAndReplace: findAndReplaceInDocument
};

function createDocument(docName) {
  var document = DocumentApp.create(docName);
  return {id: document.getId(), url: document.getUrl()};
}

/*
 * Parse a template and extract fillable areas with their title.
 */
function extractQuestionsFromDocument(templateId) {
  var questions = [];
  var template = DocumentApp.openById(templateId);
  var body = template.getBody();
  var end = null;
  while (true) {
    var start = body.findText("{{", end);
    if (!start) {
      break;
    }
    var end = body.findText("}}", start);
    var question = "";
    if (end) { 
      question = start.getElement().asText().getText().substring(start.getEndOffsetInclusive() + 1, end.getStartOffset());
    } else {
      question = start.getElement().asText().getText().substring(start.getEndOffsetInclusive() + 1);
    }
    if (!REGEX_LIST.CURRENT_DATE_REGEX.test(question)) {
      questions.push(question);
    }
  }
  return questions;
}

/*
 * Find and replace all the placeholders.
 */

function findAndReplaceInDocument(documentId, replacementMap) {
  var requests = [];
  replacementMap["{{"] = "";
  replacementMap["}}"] = "";
  replacementMap[getStringInLocale(strCodes.CURRENT_DATE)] = formatDate(new Date());
  for (var findText in replacementMap) {
    var request = {
      replaceAllText: {
        containsText: {
          text: findText,
          matchCase: false,
        },
        replaceText: replacementMap[findText],
      }
    };
    requests.push(request);
  }
  var response = Docs.Documents.batchUpdate({'requests': requests}, documentId);
  var replies = response.replies;
  for (var i = 0; i < replies.length; i++) {
    var reply = replies[i];
    var numReplacements = reply.replaceAllText.occurrencesChanged || 0;
  }
}
