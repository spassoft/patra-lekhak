/**
 * File related utility functions.
 *
 * @author paridhi.chandra@gmail.com
 */

function removeFileById(fileId) {
  var file = DriveApp.getFileById(fileId);
  var folder = file.getParents().next();
  folder.removeFile(file);
  file.setTrashed(true);
}

function revertTempFileContent(documentId, templateId) {
  mimeHandlers[DriveApp.getFileById(documentId).getMimeType()].revert(documentId, templateId);
}