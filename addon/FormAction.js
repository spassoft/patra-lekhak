/**
 * All actions on the forms, except for letter generation are captured here.
 *
 * @author paridhi.chandra@gmail.com
 */
/*
 * 1. Create a form
 * 2. Add questions
 * 3. Ensure it has the addon required elements, and attributes.
 * 4. Setup triggers for form submission.
 */
function createForm(fileId, fileName, questions) {
  var formName = fileName.replace(REGEX_LIST.TEMPLATE_FILENAME_SUFFIX_REGEX,
                                  getStringInLocale(strCodes.FORM_FILENAME_SUFFIX));
  Logger.log("Template Name: %s, FormName: %s", fileName, formName);
  var form = FormApp.create(formName);

  for (var i = 0; i < questions.length; i++) {
    if (REGEX_LIST.DATE_REGEX.test(questions[i])) {
      form.addDateItem().setTitle(questions[i]);
    } else {
      form.addTextItem().setTitle(questions[i]);
    }
  }

  setupTriggers(form.getId());
  return form.getId();
}

/*
 * When a form is deleted, its corresponding trigger should go.
 */
function deleteTrigger(formId) {
  var triggers = ScriptApp.getProjectTriggers();
  for (var i = 0; i < triggers.length; i++) {
    if (triggers[i].getTriggerSourceId() == formId) {
      ScriptApp.deleteTrigger(triggers[i]);
    }
  }
}

/*
 * When a form is created, associate a trigger
 */
function setupTriggers(formId) {
  ScriptApp.newTrigger("onFormSubmit")
           .forForm(FormApp.openById(formId))
           .onFormSubmit()
           .create();
}
