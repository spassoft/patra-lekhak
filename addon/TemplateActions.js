/**
 * This file contains all the actions on templates.
 *
 * @author paridhi.chandra@gmail.com
 */

/*
 * Figure out which templates don't have a form and generate for each.
 */
function convertTemplatesToForms() {
  var list = getTemplatesWithoutForm();
  if (!list || list.length == 0) {
    return;
  }
  for (var i = 0; i < list.length; i++) {
    processOneTemplate(list[i], DriveApp.getFileById(list[i]).getName());
  }
}

/*
 * Process one template to generate a form.
 */
function processOneTemplate(fileId, fileName) {
  if (validateSelectedItem(fileId, fileName)) {
    var questions = mimeHandlers[DriveApp.getFileById(fileId).getMimeType()].extractQuestions(fileId);
    if (questions.length == 0) {
      throw Error(getStringInLocale(strCodes.ERR_EMPTY_TEMPLATE));
    }
    var formId = createForm(fileId, fileName, questions);
    var ssformId = createSSForm(fileId, fileName, questions);
    addForm(formId, ssformId, fileId);
    removeTemplateWithoutForm(fileId);
  }
}

/*
 * Ensure that the template is a valid Patra-Lekhak template.
 */
function validateSelectedItem(fileId, fileName) {
  var templateFile = DriveApp.getFileById(fileId);
  if (templateFile.getMimeType() != MimeType.GOOGLE_DOCS) {
    throw new Error(getStringInLocale(strCodes.ERR_NOT_A_DOCUMENT));
  }
  var mapping = getMappingForTemplate(fileId);
  if (mapping) {
    Logger.log(fileId);
    removeTemplateWithoutForm(fileId);
    Logger.log(getStringInLocale(strCodes.ERR_FORM_ALREADY_EXISTS), DriveApp.getFileById(mapping.formId).getUrl());
    return false;
  }
  return true;
}
