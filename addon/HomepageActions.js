/*
 * This file creates an event handler to show the homepage of the addon when
 * the addon icon is clicked.
 *
 * @author paridhi.chandra@gmail.com
 */

var mimeHandlers = {};
/*
 * Action handler when our add-on is triggered.
 */
function onHomepage(e) {
  var err = null;
  try {
    cleanupMap();
    convertTemplatesToForms();
  } catch (error) {
    err = error;
    Logger.log(error);
  }
  return createHomepage(err);
}

/*
 * Action handler when user wants to create a template.
 */
function onCreateTemplateRequest(e) {
  if (e.formInput.templateName === "") {
    e.formInput.templateName = getStringInLocale(strCodes.DEFAULT_TEMPLATE_NAME);
  }
  var fileName = e.formInput.templateName + getStringInLocale(strCodes.TEMPLATE_FILENAME_SUFFIX);
  var file = mimeHandlers[MimeType.GOOGLE_DOCS].create(fileName);
  addTemplateWithoutForm(file.id);
  return createOpenDocumentActionResponse(file.url);
}

/*
 * Action handle when user wants to fill a form.
 */
function onFormClicked(e) {
  return createOpenDocumentActionResponse(FormApp.openById(e.parameters.fileId)
                                                 .getPublishedUrl());
}

/*
 * Action handle when user wants to fill a spreadsheet based form.
 */
function onSSFormClicked(e) {
  return createOpenDocumentActionResponse(SpreadsheetApp.openById(e.parameters.fileId)
                                                        .getUrl());
}
