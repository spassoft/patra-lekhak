/**
 * All actions on the spreadsheet forms for management purpose are captured here.
 *
 * @author paridhi.chandra@gmail.com
 */
/*
 * 1. Create a spreadsheet form
 * 2. Add questions
 * 3. Ensure it has the addon required elements, and attributes.
 * 4. Setup triggers for form submission.
 */
function createSSForm(fileId, fileName, questions) {
  var formName = fileName.replace(REGEX_LIST.TEMPLATE_FILENAME_SUFFIX_REGEX,
                                  getStringInLocale(strCodes.SS_FORM_FILENAME_SUFFIX));
  Logger.log("Template Name: %s, FormName: %s", fileName, formName);
  var spreadsheet = SpreadsheetApp.create(formName);
  var sheet = spreadsheet.getSheetByName("Sheet1")
                         .setName(getStringInLocale(strCodes.FORM));
  sheet.appendRow(questions);
  sheet.getDataRange().setFontStyle("italic").setFontWeight("bold");
  sheet.setColumnWidths(1, questions.length, 300);
  var logSheet = spreadsheet.insertSheet(getStringInLocale(strCodes.LOG));
  logSheet.appendRow(questions);
  logSheet.getDataRange().setFontStyle("italic").setFontWeight("bold");
  logSheet.setColumnWidths(1, questions.length, 300);
  return spreadsheet.getId();
}
