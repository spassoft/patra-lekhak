/**
 * This file contains code for showing complete lists which were truncated.
 *
 * @author paridhi.chandra@gmail.com
 */

function onFormListMoreClicked(e) {
  // Create peek header
  var peekHeader = CardService.newCardHeader()
                              .setTitle(getStringInLocale(strCodes.APP_NAME));
  var formSection = createFormsSection(false);
  return CardService.newCardBuilder()
                    .setPeekCardHeader(peekHeader)
                    .addSection(formSection)
                    .build();
}

function onSSFormListMoreClicked(e) {
  // Create peek header
  var peekHeader = CardService.newCardHeader()
                              .setTitle(getStringInLocale(strCodes.APP_NAME));
  var formSection = createSSFormsSection(false);
  return CardService.newCardBuilder()
                    .setPeekCardHeader(peekHeader)
                    .addSection(formSection)
                    .build();
}

function onGenerateFromSSFormListMoreClicked(e) {
  // Create peek header
  var peekHeader = CardService.newCardHeader()
                              .setTitle(getStringInLocale(strCodes.APP_NAME));
  var formSection = createGenerateFromSSFormsSection(false);
  return CardService.newCardBuilder()
                    .setPeekCardHeader(peekHeader)
                    .addSection(formSection)
                    .build();
}
