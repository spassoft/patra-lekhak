/**
 * This file contains code for creating UI on the homepage.
 *
 * @author paridhi.chandra@gmail.com
 */

/*
 * Main homepage
 */
function createHomepage(error) {
  var section = null;
  if (error) {
    section = createErrorSection(getStringInLocale(strCodes.ERR_TEMPLATE_FILE) + " " + error);
  }
  var newTemplateSection = createNewTemplateSection();
  // Create peek header
  var peekHeader = CardService.newCardHeader()
                              .setTitle(getStringInLocale(strCodes.APP_NAME));
  // assemble everything and return
  var card = CardService.newCardBuilder()
                        .setPeekCardHeader(peekHeader);
  if(section) {
    card.addSection(section);
  }
  card.addSection(newTemplateSection);

  var formsSection = createFormsSection(true);
  if (formsSection) {
    card.addSection(formsSection);
  }

  var ssformSection = createSSFormsSection(true);
  if (ssformSection) {
    card.addSection(ssformSection);
  }
  return card.build();
}
/*
 * Section on the homepage which has form to perform management actions:
 * e.g., create new template
 */
function createNewTemplateSection() {
  // Create an action
  var action = CardService.newAction()
                          .setFunctionName('onCreateTemplateRequest');
  var textInput = CardService.newTextInput()
                             .setFieldName("templateName")
                             .setTitle(getStringInLocale(strCodes.UI_TEMPLATE_NAME_TITLE))
                             .setHint(getStringInLocale(strCodes.UI_TEMPLATE_NAME_HINT));
  var button = CardService.newTextButton()
                          .setText(getStringInLocale(strCodes.UI_BUTTON_CREATE_TEMPLATE))
                          .setOnClickAction(action);
  var section = CardService.newCardSection()
                           .setHeader(getStringInLocale(strCodes.UI_TEMPLATE_SECTION_HEADING))
                           .addWidget(textInput)
                           .addWidget(button);
  return section;
}

/*
 * Create a section that lists all forms to be used for document creation.
 */
function createFormsSection(showTruncatedList) {
  return createListBasedSection({
    list: getForms(),
    buttonSet: [{
      text: "fill",
      callback: "onFormClicked"
    }],
    headerString: getStringInLocale(strCodes.UI_FORM_SECTION_HEADING),
    moreCallback: "onFormListMoreClicked",
    fileNameSpliceRegex: REGEX_LIST.FORM_FILENAME_SUFFIX_REGEX,
    showTruncatedList: showTruncatedList
  });
}

/*
 * Create a section that lists all spreadsheet forms to be used for document creation.
 */
function createSSFormsSection(showTruncatedList) {
  return createListBasedSection({
    list: getSSForms(),
    buttonSet: [
      {
        text: "fill",
        callback: "onSSFormClicked"
      },
      {
        text: getStringInLocale(strCodes.UI_GENERATE_ACTION),
        callback: "onGenerateFromSSForm"
      }
    ],
    headerString: getStringInLocale(strCodes.UI_SSFORM_SECTION_HEADING),
    moreCallback: "onSSFormListMoreClicked",
    fileNameSpliceRegex: REGEX_LIST.SSFORM_FILENAME_SUFFIX_REGEX,
    showTruncatedList: showTruncatedList
  });
}
