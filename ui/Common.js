/**
 * This file contains function that buld UI fragments that are used
 * commonly at different places.
 *
 * @author paridhi.chandra@gmail.com
 */
/*
 * Section to respond any action that leads to opening a document
 * e.g., editing a template, filling a form
 */
function createOpenDocumentActionResponse(url) {
  return CardService.newActionResponseBuilder()
                    .setOpenLink(CardService.newOpenLink()
                                 .setUrl(url)
                                 .setOpenAs(CardService.OpenAs.FULL_SIZE)
                                 .setOnClose(CardService.OnClose.RELOAD_ADD_ON))
                    .build();
}

/*
 * Section to display error.
 */
function createErrorSection(errorText) {
  return CardService.newCardSection()
                    .addWidget(CardService.newTextParagraph()
                                          .setText(errorText));
}

/*
 * Section based on some list. This is mainly used to list down known forms.
 */
function createListBasedSection(params) {
  var section = CardService.newCardSection();

  if (!params.list || params.list.length == 0) {
    return null;
  }
  var count = 0;
  section.setHeader(params.headerString);
  var listCount = (params.list.length > serviceConfig.truncatedListSize &&
                   params.showTruncatedList) ?
      serviceConfig.truncatedListSize : params.list.length;
  for (var i = 0; i < listCount; i++) {
    var id = params.list[i];
    var document = DriveApp.getFileById(id);
    var buttonset = CardService.newButtonSet();
    for (var j = 0; j < params.buttonSet.length; j++) {
      buttonset.addButton(createButton({
        text: (params.buttonSet[j].text == "fill" ?
              document.getName().replace(params.fileNameSpliceRegex, "") :
              params.buttonSet[j].text),
        callback: params.buttonSet[j].callback,
        callbackParams: {
          fileId: id,
          fileName: document.getName()
        }
      }));
    }
    section.addWidget(buttonset);
    count++;
  }
  if (count == 0) {
    return null;
  } else if (params.showTruncatedList &&
             params.list.length > serviceConfig.truncatedListSize) {
    section.addWidget(CardService.newTextButton()
                                 .setText(getStringInLocale(strCodes.UI_MORE_BUTTON))
                                 .setOnClickAction(CardService.newAction().setFunctionName(params.moreCallback)));
  }
  return section;
}

/*
 * Create a button based on given configuration.
 */
function createButton(params) {
  return CardService.newTextButton()
                    .setText(params.text)
                    .setOnClickAction(CardService.newAction()
                                                 .setFunctionName(params.callback)
                                                 .setParameters(params.callbackParams));
}