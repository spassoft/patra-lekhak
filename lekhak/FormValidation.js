/**
 * Validations to be performed before acting on a form.
 *
 * @author paridhi.chandra@gmail.com
 */
function validateForm(formId) {
  try {
    var form = FormApp.openById(formId);
  } catch (error) {
    Logger.log(strCode.ERR_MISSING_FORM + formId);
    throw Error(strCode.ERR_MISSING_FORM + formId + " " + error);
  }
  if (!getMappingForForm(formId)) {
    throw Error(getStringInLocale(strCodes.ERR_NOT_PL_FORM));
  }
}