/**
 * Functions to generate letters when a bulk generation request via spreadsheet is fired.
 *
 * @author paridhi.chandra@gmail.com
 */
/*
 * Action handler to trigger the magic when spreadsheet based bulk generation is triggered.
 */
var SS_EMPTY_ERROR_MSG = "No more elements";

function onGenerateFromSSForm(e) {
  enqueueTT({id: e.commonEventObject.parameters.fileId,
             func: "onSSTimeTrigger"
            });
}

function onSSTimeTrigger(element) {
  var ssId = element.id;
  try {
    processOneSSRow(ssId);
    processOneSSRow(ssId);
    // Make this spreadsheet most recently used.
    reshuffleSSFormList(ssId);
  } catch (error) {
    if (error.message === SS_EMPTY_ERROR_MSG) {
      dequeueTT({id: ssId});
    } else {
      throw error;
    }
  }
}

function processOneSSRow(ssId) {
  var mailRecipient = Session.getActiveUser().getEmail();
  var templateId = getMappingForSSForm(ssId).templateId;
  var spreadsheet = SpreadsheetApp.openById(ssId);
  var sheet = spreadsheet.getSheetByName(getStringInLocale(strCodes.FORM));
  var logSheet = spreadsheet.getSheetByName(getStringInLocale(strCodes.LOG));
  var filledRange = sheet.getDataRange();
  var header = sheet.getRange(1, 1, 1, filledRange.getNumColumns());
  var headerValues = header.getValues()[0];
  if (filledRange.getNumRows() <= 1) {
    throw Error(SS_EMPTY_ERROR_MSG);
  }
  var row = sheet.getRange(2, 1, filledRange.getNumRows() - 1, filledRange.getNumColumns()).getValues()[0];
  var documentId = getTemporaryDocumentId(templateId);
  var replaceMap = extractDataFromRow(row, headerValues);
  generateOneLetter(documentId, replaceMap, mailRecipient);
  removeFileById(documentId);
  logSheet.appendRow(row);
  sheet.deleteRow(2);
}

function extractDataFromRow(row, header) {
  var response = {};
  for (var i = 0; i < header.length; i++) {
    if (row[i] instanceof Date) {
      response[header[i]] = Utilities.formatDate(row[i], Session.getScriptTimeZone(), "dd-MMM-yyyy");
    } else {
      response[header[i]] = row[i];
    }
  }
  return response;
}
