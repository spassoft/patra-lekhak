/**
 * Functions to generate letters when a single letter form is filled.
 *
 * @author paridhi.chandra@gmail.com
 */
/*
 * Action handler to trigger the magic when form is submitted.
 */
function onFormSubmit(e) {
  var mailRecipient = Session.getActiveUser().getEmail();
  var listOfDateItems = getDateItemNames(e.source);
  var replaceMap = extractDataFromEvent(e.response, listOfDateItems);
  var templateId = getMappingForForm(e.source.getId()).templateId;
  var documentId = getTemporaryDocumentId(templateId);
  generateOneLetter(documentId, replaceMap, mailRecipient);
  removeFileById(documentId);
  // Make this form most recently used.
  reshuffleFormList(e.source.getId());
}

function getDateItemNames(form) {
  return list = form.getItems(FormApp.ItemType.DATE);
}

function extractDataFromEvent(formResponse, listOfDateItems) {
  var replacements = {};
  responses = formResponse.getItemResponses();
  for (var i = 0; i < responses.length; i++) {
    var response = responses[i];
    var isDate = false;
    for (var j = 0; j < listOfDateItems.length; j++) {
      if(response.getItem().getTitle() == listOfDateItems[j].getTitle()) {
        isDate = true;
        var dash = getStringInLocale(strCodes.DATE_SEPARATOR);
        replacements[response.getItem().getTitle()] = response.getResponse().split(dash).reverse().join(dash);
        break;
      }
    }
    if (!isDate) {
      replacements[response.getItem().getTitle()] = response.getResponse();
    }
  }
  for (var findText in replacements) {
    Logger.log("%s : %s", findText, replacements[findText]);
  }
  return replacements;
}
