/**
 * Functions required to generate a letter.
 *
 * @author paridhi.chandra@gmail.com
 */
function generateOneLetter(documentId, replaceMap, mailRecipient) {
  try {
    mimeHandlers[DriveApp.getFileById(documentId).getMimeType()].findAndReplace(documentId, replaceMap);
    var pdfId = generatePdf(documentId);
    sendEmail(mailRecipient, pdfId);
    removeFileById(pdfId);
  } catch (error) {
    Logger.log(error.toString());
    MailApp.sendEmail(mailRecipient, getStringInLocale(strCodes.ERROR_MAIL_SUBJECT), error.toString());
  }
}

function getTemporaryDocumentId(templateId) {
  template = DriveApp.getFileById(templateId);
  tempName = template.getName() + getStringInLocale(strCodes.FILE_NAME_SEPARATOR) + (new Date()).toISOString();
  newDoc = template.makeCopy(tempName);
  return newDoc.getId();
}

function generatePdf(documentId) {
  doc = DriveApp.getFileById(documentId);
  var pdf = DriveApp.createFile(doc.getBlob());
  var pdf_name = getStringInLocale(strCodes.GENERATED_FILE_PREFIX) + formatDate(new Date()) + ".pdf";
  pdf.setName(pdf_name);
  return pdf.getId();
}

function sendEmail(mailRecipient, documentId) {
  MailApp.sendEmail({
    to: mailRecipient,
    subject: getStringInLocale(strCodes.MAIL_SUBJECT),
    body: getStringInLocale(strCodes.MAIL_BODY),
    attachments: [
      DriveApp.getFileById(documentId).getBlob(),
    ],
  });
}